# Shiny Numbers Demo
Write code to return the first 100 shiny numbers greater than n. 
A shiny number is a number where each base-10 digit (from left to right) is ≤ the next digit.

* Shiny: 123, 499, 111, 0, 246888
* Not Shiny: 1231, 21, 63

## Running
Execute `./gradlew run --args='123'` where `123` is your starting number. 

The first 100 shiny numbers greater than `123` will be printed out to the console in a string 
separated by commas.