package com.yogasoft;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

  public static void main(String[] args) {
    if (args.length < 1) {
      throw new RuntimeException("One number required as argument.");
    }
    try {
      findAndPrintShinyNumbers(Integer.parseInt(args[0]));
    } catch (NumberFormatException ex) {
      throw new RuntimeException("Valid integer number required.", ex);
    }
  }

  /**
   * Finds the first 100 numbers greater than the passed in number where each base-10 digit (from
   * left to right) is <= the next digit.
   *
   * Assumes that all single-digit numbers are shiny.
   */
  private static void findAndPrintShinyNumbers(int startAfterNumber) {
    // Increment startAfterNumber by one since we don't include it in the results.
    System.out.println(IntStream.iterate(startAfterNumber + 1, i -> ++i).filter(i -> {
      // Handle single-digit case - always shiny
      if(i % 10 == i) {
        return true;
      }
      AtomicBoolean isShiny = new AtomicBoolean(true);
      String.valueOf(i).chars().map(Character::getNumericValue).reduce((a, b) -> {
        if(a > b){
          isShiny.set(false);
        }
        return b;
      });
      return isShiny.get();
    }).limit(100).mapToObj(Integer::toString).collect(Collectors.joining(", ")));
  }
}
